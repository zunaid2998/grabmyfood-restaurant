import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { HttpClientModule } from '@angular/common/http';
import { MyApp } from './app.component';

import { CategoryPage } from '../pages/category/category';
import { ProfilePage } from '../pages/profile/profile';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginPage } from '../pages/login/login';
import { ReportPage } from '../pages/report/report';
import { OrderPage } from '../pages/order/order';
import { RestaurantServiceProvider } from '../providers/restaurant-service';
import { CategoryDetailPage } from '../pages/category-detail/category-detail';
import { MenuDetailPage } from '../pages/menu-detail/menu-detail';

import { Camera } from '@ionic-native/camera';
import { MenuChoicePage } from '../pages/menu-choice/menu-choice';

@NgModule({
  declarations: [
    MyApp,
    CategoryPage,
    ProfilePage,
    HomePage,
    TabsPage,
    LoginPage,
    ReportPage,
    OrderPage,
    CategoryDetailPage,
    MenuDetailPage,
    MenuChoicePage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    CategoryPage,
    ProfilePage,
    HomePage,
    TabsPage,
    LoginPage,
    ReportPage,
    OrderPage,
    CategoryDetailPage,
    MenuDetailPage,
    MenuChoicePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    RestaurantServiceProvider,
    Camera
  ]
})
export class AppModule {}
