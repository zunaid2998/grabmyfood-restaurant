import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Restaurant, Category } from '../core/model';

@Injectable()
export class RestaurantServiceProvider {

  private _currentRestaurantId: number;
  public get currentRestaurantId(): number {
    return this._currentRestaurantId;
  }
  public set currentRestaurantId(value: number) {
    this._currentRestaurantId = value;
  }

  apiUrl: string = "http://grabmyfood.ahmedzunaid.com/api/restaurant";
  
  constructor(public http: HttpClient) {}

  getRestaurantDetail(id: number): Observable<Restaurant>{
    return this.http.get<Restaurant>(`${this.apiUrl}/${id}/menu`)
  }

  createCategory(restaurantId: number, categoryName: string): Observable<Category[]>{
    return this.http.post<Category[]>(`${this.apiUrl}/${restaurantId}/category/create`, {categoryName: categoryName})
  }

}
