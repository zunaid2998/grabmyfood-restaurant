import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { App } from 'ionic-angular';

@Component({
  selector: 'page-order',
  templateUrl: 'order.html',
})
export class OrderPage {
  orderType: string;
  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              public viewCtrl: ViewController, 
              public app: App) {
    this.orderType = "liveOrders";
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OrderPage');
  }

  onClickBackButton(){
    this.app.getRootNav().pop();
    this.navCtrl.setRoot(HomePage);
  }

}
