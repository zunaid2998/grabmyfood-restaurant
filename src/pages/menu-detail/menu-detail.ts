import { Component } from '@angular/core';
import { NavController, NavParams, ActionSheetController, ModalController } from 'ionic-angular';
import { Menu, MenuChoice } from '../../core/model';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { MenuChoicePage } from '../menu-choice/menu-choice';

@Component({
  selector: 'page-menu-detail',
  templateUrl: 'menu-detail.html',
})
export class MenuDetailPage {
  myphoto: any;
  menu: Menu;
  mode: string;
  menuChoices: string[];
  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public camera: Camera,
              public actionSheetCtrl: ActionSheetController,
              public modalCtrl: ModalController) {
    this.mode = this.navParams.get('mode');
    this.menu = this.navParams.get('menu');
    if(this.menu){
      /*this.menu.menuChoices.map(x=>{
        let value: string = x.value;
        x['choices'] = value.split('|');
      })*/
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MenuDetailPage');
  }
  
  save(){

  }

  onClickAddMenuOption(){
    this.modalCtrl.create(MenuChoicePage, {menuChoice: {}, mode: 'create'}).present();
  }

  onClickEdit(menuChoice: MenuChoice){
    console.log(menuChoice);
    this.modalCtrl.create(MenuChoicePage, {menuChoice: menuChoice}).present();

  }

  onClickDelete(menuChoice: MenuChoice){
    console.log(menuChoice);
  }

  openCameraOptions() {
      let actionSheet = this.actionSheetCtrl.create({
        title: 'Option',
        buttons: [
          {
            text: 'Take photo',
            handler: () => {
              this.takePhoto();
            }
          },
          {
            text: 'Choose photo from Gallery',
            handler: () => {
              this.openGallery();
            }
          },
        ]
      });
    actionSheet.present();
  }

  openGallery(){
    this.camera.cleanup().then(()=> {
      const options: CameraOptions = {
        quality: 100,
        destinationType: this.camera.DestinationType.DATA_URL,
        sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        saveToPhotoAlbum: false,
        allowEdit: true,
        targetWidth: 300,
        targetHeight: 300
      }
      
      this.camera.getPicture(options).then((imageData) => {
       this.myphoto = 'data:image/jpeg;base64,' + imageData;
      }, error => console.log(error));
    })  
    
  }

  takePhoto(){
    this.camera.cleanup().then(()=>{
      const options: CameraOptions = {
        quality: 100,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        allowEdit: true
      }
      
      this.camera.getPicture(options).then((imageData) => {
       this.myphoto = 'data:image/jpeg;base64,' + imageData;
      }, error => console.log(error));
    })
  }

}
