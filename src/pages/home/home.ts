import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { Restaurant, Owner } from '../../core/model';
import { HttpClient } from '@angular/common/http';
import { TabsPage } from '../tabs/tabs';
import { RestaurantServiceProvider } from '../../providers/restaurant-service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  restaurants: Restaurant[];
  owner: Owner;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              public http: HttpClient,
              public alertCtrl: AlertController,
              public restaurantService: RestaurantServiceProvider) {
    this.restaurants = this.navParams.get('restaurants');
    this.owner = this.navParams.get('owner');
  }

  onClickRestaurant(restaurant: Restaurant){
    this.restaurantService.currentRestaurantId = restaurant.id;
    this.navCtrl.push(TabsPage);
  }

  logout(){
    this.alertCtrl.create({
      title: 'Logout!',
      message: 'Are you sure you want to logout?',
      buttons: [{
        text: 'Stay',
        role: 'Cancel'
      }, {
        text: 'logout',
        handler: () => {
          this.navCtrl.pop();
        }
      }]
    }).present();
  }

}
