import { Component } from '@angular/core';

import { CategoryPage } from '../category/category';
import { ProfilePage } from '../profile/profile';
import { ReportPage } from '../report/report';
import { OrderPage } from '../order/order';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = OrderPage;
  tab2Root = CategoryPage;
  tab3Root = ProfilePage;
  tab4Root = ReportPage;

  constructor() {}
}
