import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Category, Menu } from '../../core/model';
import { MenuDetailPage } from '../menu-detail/menu-detail';

@Component({
  selector: 'page-category-detail',
  templateUrl: 'category-detail.html',
})
export class CategoryDetailPage implements OnInit {
  category: Category;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.category = this.navParams.get('category');
  }
  
  ngOnInit() {
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CategoryDetailPage');
  }

  onClickAddMenu(){
    this.navCtrl.push(MenuDetailPage, { mode: 'create', menu: {} });
  }

  onSelectedMenu(menu: Menu){
    this.navCtrl.push(MenuDetailPage, { mode: 'edit', menu: menu });
  }

}
