import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { MenuChoice } from '../../core/model';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'page-menu-choice',
  templateUrl: 'menu-choice.html',
})
export class MenuChoicePage {
  menuChoice: MenuChoice;
  menuChoiceValueText: string = null;
  menuChoiceValuePrice: number = null;
  mode: string;
  
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.menuChoice = this.navParams.get('menuChoice');
    this.mode = this.navParams.get('mode');
    if(this.mode === 'create'){
      this.menuChoice = new MenuChoice();
    }
  }

  closeModal(){
    this.navCtrl.pop();
  }

  onClickAddAnotherValue(f: NgForm){
    console.log(f);
  }

}
