import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { HttpClient } from '../../../node_modules/@angular/common/http';
import { NgForm } from '@angular/forms';
import { HomePage } from '../home/home';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              public loadingCtrl: LoadingController,
              public http: HttpClient,
              public toastCtrl: ToastController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  loginButtonClicked(f : NgForm){
    const loadCtrl = this.loadingCtrl.create({
      content: "Validating User ..."
    });
    loadCtrl.present();
    let body : any = {
      email: f.value.email,
      password: f.value.password
    }
    this.http.post('http://grabmyfood.ahmedzunaid.com/api/authenticate', body )
      .subscribe( (response : any) => {
        loadCtrl.dismiss();
        this.navCtrl.push(HomePage, { restaurants : response.restaurants, owner: response.owner });
      }, (error) => {
        if(error.error.text === 'user_not_found'){
          loadCtrl.dismiss();
          this.showAlert('User not found. Please try again');
        }
        if(error.error.text === 'user_inactive'){
          loadCtrl.dismiss();
          this.showAlert('User is not active. Please try again');
        }
      })
  }

  showAlert(message: string) : void {
    const toast = this.toastCtrl.create({
      message: message,
      duration: 4000
    });
    toast.present();
  }

}
