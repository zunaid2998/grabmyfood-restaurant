import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { Restaurant, Category } from '../../core/model';
import { RestaurantServiceProvider } from '../../providers/restaurant-service';
import { CategoryDetailPage } from '../category-detail/category-detail';

@Component({
  selector: 'page-category',
  templateUrl: 'category.html'
})
export class CategoryPage implements OnInit {
  restaurant: Restaurant;
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public restaurantService: RestaurantServiceProvider,
              public alertCtrl: AlertController) {}

  ngOnInit(){
    let restaurantId = this.restaurantService.currentRestaurantId;
    if(restaurantId){
      this.restaurantService.getRestaurantDetail(restaurantId)
        .subscribe((data: any) => { 
          this.restaurant = data.restaurant;
        }, (error)=> console.log(error));
    }
  }

  onSelectCategory(category: Category){
    this.navCtrl.push(CategoryDetailPage, { category: category });
  }

  addCategory(){
    const prompt = this.alertCtrl.create({
      title: 'Add new cateogry',
      message: "Enter a name for this new category you're so keen on adding",
      inputs: [
        {
          name: 'categoryName',
          placeholder: 'Category Name'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Add',
          handler: data => {
            this.restaurantService.createCategory(this.restaurant.id, data.categoryName)
              .subscribe((categories: Category[]) => {
                this.restaurant.categories = categories;
              },error => console.log(error));
          }
        }
      ]
    });
    prompt.present();
  }

}
